package main

import (
	"gitlab.com/friendly-security/sec-helper/cmd"
)

func main() {
	/* #nosec */
	cmd.Execute()
}
