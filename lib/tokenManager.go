package lib

import (
	"encoding/json"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"time"

	"github.com/aws/aws-sdk-go-v2/service/sts/types"
	"github.com/golang-utils/lockfile"
)

type TokenManager struct {
	lock            lockfile.LockFile
	credentialsPath string
	lockPath        string
}

func NewTokenManager() (*TokenManager, error) {

	usr, _ := user.Current()
	return &TokenManager{
		lock:            lockfile.New(),
		credentialsPath: filepath.Join(usr.HomeDir, ".sec-helper", "cred"),
		lockPath:        filepath.Join(os.TempDir(), "sec-helper.lock"),
	}, nil
}

func (tw *TokenManager) WriteToken(cred *types.Credentials) error {
	tw.acquire_lock()
	defer tw.release_lock()

	data, err := json.MarshalIndent(cred, "", "")
	if err != nil {
		return err
	}

	err = os.WriteFile(tw.credentialsPath, data, 0600)
	if err != nil {
		return err
	}

	fmt.Printf("Wrote session token")
	fmt.Printf("Token is valid until: %v\n", cred.Expiration)

	return nil
}

func (tw *TokenManager) ReadToken() (*types.Credentials, error) {

	data, err := os.ReadFile(tw.credentialsPath)
	if err != nil {
		return nil, err
	}

	creds := types.Credentials{}

	err = json.Unmarshal(data, &creds)
	if err != nil {
		return nil, err
	}

	return &creds, nil
}

func (tw *TokenManager) acquire_lock() {
	for {
		if err := tw.lock.Lock(tw.lockPath); err == nil {
			return
		} else {
			fmt.Printf("Waiting for lock %s\n", tw.lockPath)
			time.Sleep(time.Second)
		}
	}
}

func (tw *TokenManager) release_lock() {
	os.Remove(tw.lockPath)
}
