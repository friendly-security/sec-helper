package lib

import (
	"github.com/spf13/cobra"
)

type CobraCompatibleFunction func(cmd *cobra.Command, args []string)
type CobraAwsAbstractor func(cmd *cobra.Command, args []string) map[string]interface{}

type account struct {
	Role   string
	Region string
	Number string
}

type baseProfile struct {
	Name string
	Mfa  string
}
type Configuration struct {
	BaseProfile baseProfile `mapstructure:"baseProfile"`
	Accounts    []account
}
