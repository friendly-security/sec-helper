package lib

func Sum(slc []int) int {
	res := 0
	for _, val := range slc {
		res += val
	}
	return res
}
