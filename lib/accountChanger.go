package lib

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/sts"
	"github.com/aws/aws-sdk-go-v2/service/sts/types"
	"github.com/spf13/cobra"
)

type AccountConfig struct {
	RoleName       string
	AccountNumber  string
	BaseProfile    string
	Region         string
	MfaTokenSerial string
}

var intmTkn *types.Credentials

func ChangeAccount(ac AccountConfig) *types.Credentials {
	cfg, err := config.LoadDefaultConfig(context.Background(), config.WithSharedConfigProfile(ac.BaseProfile), config.WithRegion(ac.Region))
	cobra.CheckErr(err)

	intermediateSvc := sts.NewFromConfig(cfg)
	if intmTkn == nil {
		tw, err := NewTokenManager()
		cobra.CheckErr(err)

		token, err := tw.ReadToken()
		cobra.CheckErr(err)

		if token != nil && token.Expiration.After(time.Now()) {
			intmTkn = token
		} else {
			intmTkn = getSessionToken(context.Background(), intermediateSvc, ac.MfaTokenSerial)
			tw.WriteToken(intmTkn)
		}
	}

	awsCreds := credentials.NewStaticCredentialsProvider(*intmTkn.AccessKeyId, *intmTkn.SecretAccessKey, *intmTkn.SessionToken)
	svc := sts.NewFromConfig(aws.Config{Credentials: awsCreds, Region: ac.Region})
	sessionName := "sec_helper_session"
	result, err := svc.AssumeRole(context.Background(), &sts.AssumeRoleInput{
		RoleArn:         constructRoleArn(ac.AccountNumber, ac.RoleName),
		RoleSessionName: &sessionName,
	})

	cobra.CheckErr(err)

	return result.Credentials
}

func constructRoleArn(accNo string, roleName string) *string {
	arn := fmt.Sprintf("arn:aws:iam::%s:role/%s", accNo, roleName)
	return &arn
}

func getSessionToken(ctx context.Context, svc *sts.Client, tokenSerialNumber string) *types.Credentials {
	tokenCode := getTokenCode(tokenSerialNumber)
	output, err := svc.GetSessionToken(ctx, &sts.GetSessionTokenInput{
		// * For very long executions this might be a problem for reuse of the intermediate token.
		// * Let's cross that bridge if an when we get there.
		DurationSeconds: aws.Int32(int32(28800)), // 8 hours
		TokenCode:       &tokenCode,
		SerialNumber:    &tokenSerialNumber,
	})

	cobra.CheckErr(err)

	return output.Credentials
}

func getTokenCode(tokenSerialNumber string) string {
	tokenCode := askForTokenCode(tokenSerialNumber)
	return cleanTokenCode(tokenCode)
}

func askForTokenCode(tokenSerialNumber string) string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("Enter mfa token for %s: ", tokenSerialNumber)
	if tokenCode, err := reader.ReadString('\n'); err != nil {
		cobra.CheckErr(err)
		return ""
	} else {
		return tokenCode
	}
}

func cleanTokenCode(tokenCode string) string {
	return strings.Trim(tokenCode, " \r\n")
}
