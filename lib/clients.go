package lib

import (
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/aws/aws-sdk-go-v2/service/ecs"
	"github.com/aws/aws-sdk-go-v2/service/elasticache"
	"github.com/aws/aws-sdk-go-v2/service/guardduty"
	"github.com/aws/aws-sdk-go-v2/service/rds"
	"github.com/aws/aws-sdk-go-v2/service/sts"
)

func GetGdClients() []*guardduty.Client {
	var clients []*guardduty.Client

	clConfs := GetClientConfigurationsFromConfig()

	for _, clConf := range clConfs {
		clients = append(clients, guardduty.NewFromConfig(aws.Config{Credentials: clConf.CredentialsProvider, Region: clConf.Region}))
	}

	return clients
}

func GetEc2Clients() []*ec2.Client {
	var clients []*ec2.Client

	clConfs := GetClientConfigurationsFromConfig()

	for _, clConf := range clConfs {
		clients = append(clients, ec2.NewFromConfig(aws.Config{Credentials: clConf.CredentialsProvider, Region: clConf.Region}))
	}

	return clients
}

func GetRdsClients() []*rds.Client {
	var clients []*rds.Client

	clConfs := GetClientConfigurationsFromConfig()

	for _, clConf := range clConfs {
		clients = append(clients, rds.NewFromConfig(aws.Config{Credentials: clConf.CredentialsProvider, Region: clConf.Region}))
	}

	return clients
}

func GetStsClients() []*sts.Client {
	var clients []*sts.Client

	clConfs := GetClientConfigurationsFromConfig()

	for _, clConf := range clConfs {
		clients = append(clients, sts.NewFromConfig(aws.Config{Credentials: clConf.CredentialsProvider, Region: clConf.Region}))
	}

	return clients
}

func GetEcsClients() []*ecs.Client {
	var clients []*ecs.Client

	clConfs := GetClientConfigurationsFromConfig()

	for _, clConf := range clConfs {
		clients = append(clients, ecs.NewFromConfig(aws.Config{Credentials: clConf.CredentialsProvider, Region: clConf.Region}))
	}

	return clients
}

func GetElasticacheClients() []*elasticache.Client {
	var clients []*elasticache.Client

	clConfs := GetClientConfigurationsFromConfig()

	for _, clConf := range clConfs {
		clients = append(clients, elasticache.NewFromConfig(aws.Config{Credentials: clConf.CredentialsProvider, Region: clConf.Region}))
	}

	return clients
}
