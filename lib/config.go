package lib

import (
	"context"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type ClientConfiguration struct {
	CredentialsProvider aws.CredentialsProvider
	Region              string
}

func GetClientConfigurationsFromConfig() []ClientConfiguration {
	var conf Configuration
	var result []ClientConfiguration

	err := viper.Unmarshal(&conf)
	cobra.CheckErr(err)

	if conf.Accounts != nil && !viper.GetBool("ignore-config") {

		for _, account := range conf.Accounts {

			creds := ChangeAccount(AccountConfig{Region: account.Region, BaseProfile: conf.BaseProfile.Name, RoleName: account.Role, MfaTokenSerial: conf.BaseProfile.Mfa, AccountNumber: account.Number})

			awsCreds := credentials.NewStaticCredentialsProvider(*creds.AccessKeyId, *creds.SecretAccessKey, *creds.SessionToken)

			result = append(result, ClientConfiguration{CredentialsProvider: awsCreds, Region: account.Region})

		}

	} else {
		cfg, err := config.LoadDefaultConfig(context.Background())
		cobra.CheckErr(err)

		result = append(result, ClientConfiguration{CredentialsProvider: cfg.Credentials, Region: cfg.Region})
	}

	return result
}
