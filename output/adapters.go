package output

import (
	"github.com/spf13/cobra"
	"gitlab.com/friendly-security/sec-helper/lib"
)

func OutputWrapper(f lib.CobraAwsAbstractor) lib.CobraCompatibleFunction {
	return func(cmd *cobra.Command, args []string) {
		write(f(cmd, args))
	}
}
func write(m map[string]interface{}) {
	// TODO output select logic goes here
	printJson(m)
}
