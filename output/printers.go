package output

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
	"github.com/spf13/cobra"
)

func printJson(m map[string]interface{}) {
	jsonStr, err := json.MarshalIndent(m, "", "\t")
	cobra.CheckErr(err)
	fmt.Println(string(jsonStr))
}

func printTable(m map[string]interface{}) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)

	// TODO: make sure order is same every time

	header := table.Row{}
	var keys []string

	for k := range m {
		header = append(header, k)
		keys = append(keys, k)
	}

	t.AppendHeader(header)

	var row []interface{}

	// TODO think about how this should work
	for _, key := range keys {
		row = append(row, m[key])
	}

	t.AppendRow(row)

	t.Style().Format.Header = text.FormatTitle

	t.Render()

}
