package cmd

import (
	"github.com/fatih/structs"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/friendly-security/sec-helper/lib"
	"gitlab.com/friendly-security/sec-helper/output"
)

func init() {
	rootCmd.AddCommand(showConfigCmd)
}

var showConfigCmd = &cobra.Command{
	Use:   "show-config",
	Short: "configuration",
	Run:   output.OutputWrapper(getConfig),
}

func getConfig(cmd *cobra.Command, args []string) map[string]interface{} {
	var conf lib.Configuration

	err := viper.Unmarshal(&conf)
	cobra.CheckErr(err)

	return structs.Map(conf)
}
