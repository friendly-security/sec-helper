package cmd

import (
	"context"

	"github.com/aws/aws-sdk-go-v2/service/sts"
	"github.com/spf13/cobra"
	"gitlab.com/friendly-security/sec-helper/lib"
	"gitlab.com/friendly-security/sec-helper/output"
)

func init() {
	rootCmd.AddCommand(stsCmd)
	stsCmd.AddCommand(showIdent)
}

var stsCmd = &cobra.Command{
	Use:   "sts",
	Short: "sts commands",
}

var showIdent = &cobra.Command{
	Use:   "show-identity",
	Short: "show caller identity",
	Run:   output.OutputWrapper(getCallerIdentity),
}

func getCallerIdentity(cmd *cobra.Command, args []string) map[string]interface{} {

	results := make(map[string]interface{})
	clients := lib.GetStsClients()

	input := &sts.GetCallerIdentityInput{}

	for _, client := range clients {
		result, err := client.GetCallerIdentity(context.Background(), input)
		cobra.CheckErr(err)
		results[*result.Account] = result
	}

	return results

}
