package cmd

import (
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string
var ignoreConfig bool

var rootCmd = &cobra.Command{
	Use:   "sec-helper",
	Short: "Sec-helper is a collection of tools that help the AWS security professional",
	Long: `A collection of tools that will perform various security related functions
across multiple accounts`,
}

func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default: $HOME/.sec-helper/config)")

	rootCmd.PersistentFlags().BoolVarP(&ignoreConfig, "ignore-config", "i", false, "will ignore the config file and use the current default profile for AWS")
	err := viper.BindPFlag("ignore-config", rootCmd.PersistentFlags().Lookup("ignore-config"))
	cobra.CheckErr(err)
}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)
		viper.AddConfigPath(filepath.Join(home, ".sec-helper"))
		viper.SetConfigType("yaml")
		viper.SetConfigName("config")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		log.Debug("Using config file: ", viper.ConfigFileUsed())
	}
}
