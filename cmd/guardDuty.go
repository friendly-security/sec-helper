package cmd

import (
	"context"
	"errors"
	"fmt"

	"github.com/aws/aws-sdk-go-v2/service/guardduty"
	"github.com/aws/aws-sdk-go-v2/service/guardduty/types"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/friendly-security/sec-helper/lib"
	"gitlab.com/friendly-security/sec-helper/output"
)

var showArchived bool
var severity int64

func init() {
	rootCmd.AddCommand(gdCmd)
	gdCmd.AddCommand(showFindings)

	showFindings.Flags().BoolVar(&showArchived, "show-archived", false, "show archived findings (default: false)")
	showFindings.Flags().Int64Var(&severity, "severity", 7.0, "show findings of severity >= (default: 7.0)")
}

var gdCmd = &cobra.Command{
	Use:   "gd",
	Short: "guard duty commands",
}

var showFindings = &cobra.Command{
	Use:   "show-findings",
	Short: "show guard duty finding",
	Run:   output.OutputWrapper(getFindings),
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if severity < 1 || severity > 10 {
			return errors.New("severity must be between 1 and 10")
		}
		return nil
	},
}

func getFindings(cmd *cobra.Command, args []string) map[string]interface{} {

	result := make(map[string]interface{})

	clients := lib.GetGdClients()

	for _, client := range clients {

		detectors, err := client.ListDetectors(context.Background(), &guardduty.ListDetectorsInput{})
		cobra.CheckErr(err)

		if len(detectors.DetectorIds) == 0 {
			log.Warn("No detectors found in region ")
		}

		detIds := detectors.DetectorIds
		for i := range detIds {
			findingIds := getFindingIds(client, detIds[i], showArchived, severity)
			out, err := client.GetFindings(context.Background(), &guardduty.GetFindingsInput{DetectorId: &detIds[i], FindingIds: findingIds})
			cobra.CheckErr(err)

			fndRes := make([]interface{}, 0)

			for _, finding := range out.Findings {
				fnr := map[string]string{
					"severity":  fmt.Sprint(finding.Severity),
					"title":     *finding.Title,
					"findingId": *finding.Id,
					"accountId": *finding.AccountId,
				}

				fndRes = append(fndRes, fnr)

			}

			result[detIds[i]] = fndRes
		}
	}

	return result

}

func getFindingIds(svc *guardduty.Client, detector string, showArchived bool, severity int64) []string {
	var archivedCondition []string

	if showArchived {
		archivedCondition = []string{"true"}
	} else {
		archivedCondition = []string{"false"}
	}

	findings, err := svc.ListFindings(
		context.Background(),
		&guardduty.ListFindingsInput{
			DetectorId: &detector, FindingCriteria: &types.FindingCriteria{
				Criterion: map[string]types.Condition{
					"severity":         {GreaterThanOrEqual: severity},
					"service.archived": {Equals: archivedCondition},
				}},
		})
	if err != nil {
		log.Warn("Could not get findings for", detector, "due to", err)
	}

	return findings.FindingIds
}
