package cmd

import (
	"context"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"github.com/aws/aws-sdk-go-v2/service/ecs"
	"github.com/aws/aws-sdk-go-v2/service/elasticache"
	"github.com/aws/aws-sdk-go-v2/service/rds"
	"github.com/aws/aws-sdk-go-v2/service/sts"
	"github.com/spf13/cobra"
	"gitlab.com/friendly-security/sec-helper/lib"
	"gitlab.com/friendly-security/sec-helper/output"
)

func init() {
	rootCmd.AddCommand(baseCmd)
	baseCmd.AddCommand(showInventory)
}

var baseCmd = &cobra.Command{
	Use:   "inventory",
	Short: "guard duty commands",
}

var showInventory = &cobra.Command{
	Use:   "list",
	Short: "list all inventory",
	Run:   output.OutputWrapper(getInventory),
}

func getInventory(_ *cobra.Command, _ []string) map[string]interface{} {
	result := make(map[string]interface{})

	// TODO: Make this testable
	stsClients := lib.GetStsClients()
	ec2Clients := lib.GetEc2Clients()
	rdsClients := lib.GetRdsClients()
	ecsClients := lib.GetEcsClients()
	elasticacheClients := lib.GetElasticacheClients()

	accounts := getAccounts(stsClients)
	ec2Counts := countEc2s(ec2Clients)
	rdsCounts := countRds(rdsClients)
	ecsCounts := countEcs(ecsClients)
	elasticacheCounts := countElasticache(elasticacheClients)

	for idx, account := range accounts {
		result[account] = map[string]int{
			"ec2":         ec2Counts[idx],
			"rds":         rdsCounts[idx],
			"ecs":         ecsCounts[idx],
			"elasticache": elasticacheCounts[idx],
			"total":       ec2Counts[idx] + rdsCounts[idx] + ecsCounts[idx] + elasticacheCounts[idx],
		}
	}

	if len(accounts) > 1 {
		result["total"] = map[string]int{
			"ec2":         lib.Sum(ec2Counts),
			"rds":         lib.Sum(rdsCounts),
			"ecs":         lib.Sum(ecsCounts),
			"elasticache": lib.Sum(elasticacheCounts),
			"total":       lib.Sum(ec2Counts) + lib.Sum(rdsCounts) + lib.Sum(ecsCounts) + lib.Sum(elasticacheCounts),
		}
	}

	return result
}

func getAccounts(clients []*sts.Client) []string {
	result := []string{}

	for _, client := range clients {
		val, err := client.GetCallerIdentity(context.Background(), &sts.GetCallerIdentityInput{})
		cobra.CheckErr(err)
		result = append(result, *val.Account)

	}

	return result
}

// TODO: Pagination

func countEc2s(clients []*ec2.Client) []int {
	result := []int{}
	for _, client := range clients {
		val, err := client.DescribeInstances(context.Background(), &ec2.DescribeInstancesInput{Filters: []types.Filter{
			{
				Name: aws.String("instance-state-code"),
				Values: []string{
					*aws.String("16"),
				},
			},
		},
		})

		cobra.CheckErr(err)

		result = append(result, len(val.Reservations))
	}

	return result

}

func countRds(clients []*rds.Client) []int {
	result := []int{}
	for _, client := range clients {
		val, err := client.DescribeDBInstances(context.Background(), &rds.DescribeDBInstancesInput{})
		cobra.CheckErr(err)
		result = append(result, len(val.DBInstances))
	}
	return result
}

func countEcs(clients []*ecs.Client) []int {
	results := []int{}

	for _, client := range clients {
		clusters, err := client.ListClusters(context.Background(), &ecs.ListClustersInput{})
		cobra.CheckErr(err)

		intermediateResults := []int{}
		for _, clusterArn := range clusters.ClusterArns {
			val, err := client.ListTasks(context.Background(), &ecs.ListTasksInput{Cluster: &clusterArn})
			cobra.CheckErr(err)
			intermediateResults = append(intermediateResults, len(val.TaskArns))
		}

		results = append(results, lib.Sum(intermediateResults))
	}
	return results
}

func countElasticache(clients []*elasticache.Client) []int {
	results := []int{}

	for _, client := range clients {
		val, err := client.DescribeCacheClusters(context.Background(), &elasticache.DescribeCacheClustersInput{})
		cobra.CheckErr(err)
		// TODO check values
		results = append(results, len(val.CacheClusters))
	}

	return results
}
